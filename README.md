# Snooker in matter.js and p5.js

This is Snooker written in matter.js and p5.js

## Install

Clone the repository and run a web server.

```
git clone https://gitlab.com/mygroup7230202/snooker-in-matter.js-and-p5.js.git
cd snooker-in-matter.js-and-p5.js
cd src

```

Run the web server (either use python or PHP):

### Python

```
python -m http.server
```

### PHP

```
php -S localhost:8000 -t .
```

## Run

Enter the URL "http://localhost:8000/" and enjoy the game!
