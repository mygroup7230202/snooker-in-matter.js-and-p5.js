/**
	This file is for handling the physics or mechanics of the objects.
**/

//checks collisions between two balls
function ballToBall(ballList)
{
	for (var i = 0; i < ballList.length; i++)
	{
		for (var j = 0; j < ballList.length; j++)
		{
			if (i != j)
			{
				let collision = Matter.Collision.collides(ballList[i], ballList[j]);

				if (collision != null)
				{
					let collisionImpact = ballList[i].speed + ballList[i].speed;
					let soundVol = map(collisionImpact, 1, 20, 0, 0.5);
					ballToBallSound.setVolume(soundVol); // Extension: The ball to ball sound is dependent on the impact of the collision
					ballToBallSound.play();
					ballToBallSound.setVolume(1);
				}
			}
		}
	}
}

function cueCollision(body)
{
	let collision = Matter.Collision.collides(body, cueStick);

	let magnitude = Math.sqrt((body.position.x - cueStick.position.x)**2 + (body.position.y - cueStick.position.y)**2);
	let unit_vector= {x: (body.position.x - cueStick.position.x) / magnitude, y: (body.position.y - cueStick.position.y) / magnitude};


	if (collision != null)
	{
		let pointOfCollision = {x: collision.supports[0].x, y: collision.supports[0].y};

		if (dist(pointOfCollision.x, pointOfCollision.y, cueStick.position.x, cueStick.position.y) > 100 && body.speed < 0.01)
		{
			cueToBallSound.play(); // Extension: sound when cue stick hits the cue ball
			let force_mag = collision.depth / 37.5;
			Body.applyForce(body, {x: body.position.x, y: body.position.y}, {x: force_mag * unit_vector.x, y: force_mag * unit_vector.y});
		}
	}
}

function mouseWheel(event) // rotate cue stick with mouse wheel
{
	if (event.delta > 0)
		Body.rotate(cueStick, Math.PI / 36);
	else
		Body.rotate(cueStick, -Math.PI / 36);
}

function ballinHole(ball) // returns true if ball is in one of the holes
{
	for (var i = 0; i < holes.length; i++)
	{
		if (dist(ball.position.x, ball.position.y, holes[i].position.x, holes[i].position.y) < 8)
			return true;
	}
}

function maxSpeed(ball) // stops ball from increasing the ball speed more than 10
{
	if (ball.speed > 12)
	{
		Body.setSpeed(ball, 12);
	}
}

/**
	Manually applied friction, this function is also usable but I decided to use frictionAir for friction with the table.
	Table is not a World body here, so the frictionAir property works fine for friction with the table.
 **/
function frictionTable(ball)
{
	let opposingUnitVector = {x: -ball.velocity.x / ball.speed, y: -ball.velocity.y / ball.speed};
	
	if (ball.speed > 10)
	{
		Body.setSpeed(ball, 10);
	}

	if (ball.speed > 0)
		Body.applyForce(ball, {x: ball.position.x, y: ball.position.y}, {x: 0.0001 * opposingUnitVector.x, y: 0.0001 * opposingUnitVector.y});
}
