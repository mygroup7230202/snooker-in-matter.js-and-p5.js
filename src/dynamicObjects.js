/** 
 
	This file is mainly for setting up the dynamic objects on the screen.

 **/

var allBalls;
var cueStick;
var cueBall;
var ballOptions = {restitution: 0.8, friction: 0.1, density: 0.01, frictionAir: 0.02};
var moveCB = 2; // step for moving the cue ball wit arrow keys
var detector = Detector.create();
const Moves = {red: "red", colored: "colored"}; //type of moves

function setupObjects()
{
	let redBallPos = redBallPositions(mode); // generate the positions of red balls
	let coloredBallPos = coloredBallPositions(mode); //generate the positions of colored balls

	allBalls = new AllBalls(redBallPos, coloredBallPos);
	allBalls.addAlltoWorld(); // add all balls to the engine.world

	cueStick = Bodies.rectangle(mouseX, mouseY, 200, 4, {restitution: .8, friction: 0.1, density: 0.02, isSensor: true});


	World.add(engine.world, [cueStick]);
	Detector.setBodies(detector, [allBalls.cueBall.body, allBalls.coloredBalls[0].body]);
}

function drawObjects()
{
	allBalls.drawAll(); // draw all balls 

	cueCollision(allBalls.cueBall.body); // collision between cue stick and cue ball

	// cueStick
	noStroke();
	fill(139,69,19);
	Body.setPosition(cueStick, {x: mouseX, y: mouseY}); // adjust the cue stick as per the mouse position
	drawVertices(cueStick.vertices);

	allBalls.regulateSpeedAll(); // impose speed limit on the cue ball
	allBalls.ballSink(); // action on sinking balls

	moveCueBall();

	ballToBall(allBalls.ballsInMotion());

}

// move cue ball with arrow keys
function moveCueBall()
{
	if (keyIsPressed)
	{
		if (keyCode == 37)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x - moveCB, y: cb.position.y});
		}
		else if (keyCode == 39)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x + moveCB, y: cb.position.y});
		}
		else if (keyCode == 38)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x, y: cb.position.y - moveCB});
		}
		else if (keyCode == 40)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x, y: cb.position.y + moveCB});
		}
	}
}

function redBallPositions(mode)
{
	let redBallPos = [];

	if (mode == "proper") // standard positions
	{
		for (var i = 1; i <= 5; i++)
		{
			for (var j = 0; j < i; j++)
			{
				redBallPos.push({
					x: table.position.x + tableWidth / 4 + (i - 1) * 8 * Math.sqrt(3),
					y: table.position.y - 8 * (i - 1) + 16 * j
				});
			}
		}
	}
	else if (mode == "randomRed" || mode == "random")
	{
		for (var i = 0; i < 15; i++)
		{
			let x1 = Math.ceil(map(Math.random(), 0, 1, 0, 49));
			let y1 = Math.ceil(map(Math.random(), 0, 1, 0, 24));

			redBallPos.push({
				x: table.vertices[0].x + 18 + (x1 - 1) * 16,
				y: table.vertices[0].y + 18 + (y1 - 1) * 16
			});
		}
	}

	return redBallPos;
}

function coloredBallPositions(mode)
{
	let coloredBallPos = [];

	if (mode == "proper" || mode == "randomRed")
	{
		coloredBallPos = [
			{
				x: Line[0].x,
				y: table.position.y + 75
			},
			{
				x: Line[0].x,
				y: table.position.y - 75
			},
			{
				x: Line[0].x,
				y: table.position.y
			},
			{
				x: table.position.x,
				y: table.position.y
			},
			{
				x: table.position.x + tableWidth / 4 - 16,
				y: table.position.y
			},
			{
				x: table.position.x + tableWidth / 4 + 130.42,
				y: table.position.y
			},
		];
	}
	else if (mode == "random")
	{
		for (var i = 0; i < 6; i++)
		{
			let x1 = Math.ceil(map(Math.random(), 0, 1, 0, 49));
			let y1 = Math.ceil(map(Math.random(), 0, 1, 0, 24));

			coloredBallPos.push({
				x: table.vertices[0].x + 18 + (x1 - 1) * 16,
				y: table.vertices[0].y + 18 + (y1 - 1) * 16
			});
		}
	}

	return coloredBallPos;
}
