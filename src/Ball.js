class Ball
{
	body;
	position;
	score;

	constructor(color, position, options, score)
	{
		this.color = color; 
		this.position = position;
		this.body = Bodies.circle(position.x, position.y, 8, options);
		this.score = score;
	}
	
	draw()
	{
		noStroke();
		fill(this.color);
		drawCircle(this.body.position, 8);
	}

	addtoWorld()
	{
		World.add(engine.world, [this.body]);
	}

	removeFromWorld()
	{
		World.remove(engine.world, [this.body]);
	}

	recordPosition(position)
	{
		this.position = position;
	}

	regulateSpeed()
	{
		maxSpeed(this.body);
	}

	isInMotion() // returns true if the ball has speed > 0.5
	{
		if (this.body.speed > 0.5)
			return true
		return false
	}
	/**
	positionToCenter(position) // this function is for returning a ball to the original position after illegal sinking
	{
		let magnitude = dist(table.position.x, table.position.y, position.x, position.y);
		let unit_vector = {x: (table.position.x - position.x) / magnitude, y: (table.position.y - position.y) / magnitude};

		let new_position = {x: position.x + 50 * unit_vector.x, y: position.y + 50 * unit_vector.y};
		
		return new_position;
	}
	**/
};
