/** 
	Class for handling all the balls
**/

class AllBalls
{
	ballOptions = {restitution: 0.8, friction: 0.1, density: 0.01, frictionAir: 0.02};

	#white = "#ffffff";
	#red = "#c80000";
	#colors = [
		"#dcdc12",
		"#015501",
		"#894514",
		"#0000c9",
		"#d45c98",
		"#000000"
	];

	cueBall;
	redBalls = [];
	coloredBalls = [];

	move = Moves.red;

	constructor(redBallPositions, coloredBallPositions)
	{
		this.cueBall = new Ball(this.#white, {x: Line[0].x - 75, y: table.position.y}, this.ballOptions, -1);

		for (var i = 0; i < 15; i++)
			this.redBalls.push(new Ball(this.#red, redBallPositions[i], this.ballOptions, 1));

		for (var i = 0; i < 6; i++)
			this.coloredBalls.push(new Ball(this.#colors[i], coloredBallPositions[i], this.ballOptions, i + 2));
	}

	drawAll() // draw all balls
	{
		this.cueBall.draw();

		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].draw();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			this.coloredBalls[i].draw();
		}
	}

	addAlltoWorld() // add all balls to the world
	{
		this.cueBall.addtoWorld();

		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].addtoWorld();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
			this.coloredBalls[i].addtoWorld();
	}

	regulateSpeedAll()
	{
		this.cueBall.regulateSpeed();
		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].regulateSpeed();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
			this.coloredBalls[i].regulateSpeed();
	}

	ballSink()
	{
		if (ballinHole(this.cueBall.body))
		{
			Body.setPosition(this.cueBall.body, this.cueBall.position);
			Body.setVelocity(this.cueBall.body, {x: 0, y: 0});
			totalScore -= 2;
			ballSinkSound.play(); //Extension: Sound when the ball sinks
		}

		for (var i = 0; i < this.redBalls.length; i++)
		{
			if (ballinHole(this.redBalls[i].body) && this.move == Moves.red)
			{
				this.redBalls[i].removeFromWorld();
				this.redBalls.splice(i, 1);
				ballSinkSound.play();
				totalScore++;
				this.move = Moves.colored;
			}
			else if (ballinHole(this.redBalls[i].body) && this.move == Moves.colored)
			{
				ballSinkSound.play();
				Body.setPosition(this.redBalls[i].body, this.redBalls[i].position);
				Body.setVelocity(this.redBalls[i].body, {x: 0, y: 0});
				totalScore--;
			}
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			if (ballinHole(this.coloredBalls[i].body))
			{
				if (this.move == Moves.colored)
				{
					if (this.redBalls.length == 0)
					{
						ballSinkSound.play();
						totalScore += this.coloredBalls[i].score;
						this.coloredBalls[i].removeFromWorld();
						this.coloredBalls.splice(i, 1);
					}
					else
					{
						ballSinkSound.play();
						totalScore += this.coloredBalls[i].score;
						Body.setPosition(this.coloredBalls[i].body, this.coloredBalls[i].position);
						Body.setVelocity(this.coloredBalls[i].body, {x: 0, y: 0});
						this.move = Moves.red;
					}
				}
				else
				{
					ballSinkSound.play();
					Body.setPosition(this.coloredBalls[i].body, this.coloredBalls[i].position);
					Body.setVelocity(this.coloredBalls[i].body, {x: 0, y: 0});
					totalScore--;
				}
			}
		}
	}

	ballsInMotion()
	{
		let motionBalls = []
		if (this.cueBall.isInMotion())
			motionBalls.push(this.cueBall.body);

		for (var i = 0; i < this.redBalls.length; i++)
		{
			if(this.redBalls[i].isInMotion())
				motionBalls.push(this.redBalls[i].body)
		}
		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			if(this.coloredBalls[i].isInMotion())
				motionBalls.push(this.coloredBalls[i].body)
		}
		return motionBalls;
	}
};

class Ball
{
	body;
	position;
	score;

	constructor(color, position, options, score)
	{
		this.color = color; 
		this.position = position;
		this.body = Bodies.circle(position.x, position.y, 8, options);
		this.score = score;
	}
	
	draw()
	{
		noStroke();
		fill(this.color);
		drawCircle(this.body.position, 8);
	}

	addtoWorld()
	{
		World.add(engine.world, [this.body]);
	}

	removeFromWorld()
	{
		World.remove(engine.world, [this.body]);
	}

	recordPosition(position)
	{
		this.position = position;
	}

	regulateSpeed()
	{
		maxSpeed(this.body);
	}

	isInMotion() // returns true if the ball has speed > 0.5
	{
		if (this.body.speed > 0.5)
			return true
		return false
	}
	/**
	positionToCenter(position) // this function is for returning a ball to the original position after illegal sinking
	{
		let magnitude = dist(table.position.x, table.position.y, position.x, position.y);
		let unit_vector = {x: (table.position.x - position.x) / magnitude, y: (table.position.y - position.y) / magnitude};

		let new_position = {x: position.x + 50 * unit_vector.x, y: position.y + 50 * unit_vector.y};
		
		return new_position;
	}
	**/
};

/** 
 
	This file is mainly for setting up the dynamic objects on the screen.

 **/

var allBalls;
var cueStick;
var cueBall;
var ballOptions = {restitution: 0.8, friction: 0.1, density: 0.01, frictionAir: 0.02};
var moveCB = 2; // step for moving the cue ball wit arrow keys
var detector = Detector.create();
const Moves = {red: "red", colored: "colored"}; //type of moves

function setupObjects()
{
	let redBallPos = redBallPositions(mode); // generate the positions of red balls
	let coloredBallPos = coloredBallPositions(mode); //generate the positions of colored balls

	allBalls = new AllBalls(redBallPos, coloredBallPos);
	allBalls.addAlltoWorld(); // add all balls to the engine.world

	cueStick = Bodies.rectangle(mouseX, mouseY, 200, 4, {restitution: .8, friction: 0.1, density: 0.02, isSensor: true});


	World.add(engine.world, [cueStick]);
	Detector.setBodies(detector, [allBalls.cueBall.body, allBalls.coloredBalls[0].body]);
}

function drawObjects()
{
	allBalls.drawAll(); // draw all balls 

	cueCollision(allBalls.cueBall.body); // collision between cue stick and cue ball

	// cueStick
	noStroke();
	fill(139,69,19);
	Body.setPosition(cueStick, {x: mouseX, y: mouseY}); // adjust the cue stick as per the mouse position
	drawVertices(cueStick.vertices);

	allBalls.regulateSpeedAll(); // impose speed limit on the cue ball
	allBalls.ballSink(); // action on sinking balls

	moveCueBall();

	ballToBall(allBalls.ballsInMotion());

}

// move cue ball with arrow keys
function moveCueBall()
{
	if (keyIsPressed)
	{
		if (keyCode == 37)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x - moveCB, y: cb.position.y});
		}
		else if (keyCode == 39)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x + moveCB, y: cb.position.y});
		}
		else if (keyCode == 38)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x, y: cb.position.y - moveCB});
		}
		else if (keyCode == 40)
		{
			let cb = allBalls.cueBall.body;
			Body.setPosition(cb, {x: cb.position.x, y: cb.position.y + moveCB});
		}
	}
}

function redBallPositions(mode)
{
	let redBallPos = [];

	if (mode == "proper") // standard positions
	{
		for (var i = 1; i <= 5; i++)
		{
			for (var j = 0; j < i; j++)
			{
				redBallPos.push({
					x: table.position.x + tableWidth / 4 + (i - 1) * 8 * Math.sqrt(3),
					y: table.position.y - 8 * (i - 1) + 16 * j
				});
			}
		}
	}
	else if (mode == "randomRed" || mode == "random")
	{
		for (var i = 0; i < 15; i++)
		{
			let x1 = Math.ceil(map(Math.random(), 0, 1, 0, 49));
			let y1 = Math.ceil(map(Math.random(), 0, 1, 0, 24));

			redBallPos.push({
				x: table.vertices[0].x + 18 + (x1 - 1) * 16,
				y: table.vertices[0].y + 18 + (y1 - 1) * 16
			});
		}
	}

	return redBallPos;
}

function coloredBallPositions(mode)
{
	let coloredBallPos = [];

	if (mode == "proper" || mode == "randomRed")
	{
		coloredBallPos = [
			{
				x: Line[0].x,
				y: table.position.y + 75
			},
			{
				x: Line[0].x,
				y: table.position.y - 75
			},
			{
				x: Line[0].x,
				y: table.position.y
			},
			{
				x: table.position.x,
				y: table.position.y
			},
			{
				x: table.position.x + tableWidth / 4 - 16,
				y: table.position.y
			},
			{
				x: table.position.x + tableWidth / 4 + 130.42,
				y: table.position.y
			},
		];
	}
	else if (mode == "random")
	{
		for (var i = 0; i < 6; i++)
		{
			let x1 = Math.ceil(map(Math.random(), 0, 1, 0, 49));
			let y1 = Math.ceil(map(Math.random(), 0, 1, 0, 24));

			coloredBallPos.push({
				x: table.vertices[0].x + 18 + (x1 - 1) * 16,
				y: table.vertices[0].y + 18 + (y1 - 1) * 16
			});
		}
	}

	return coloredBallPos;
}

/**
	This file is for handling the physics or mechanics of the objects.
**/

//checks collisions between two balls
function ballToBall(ballList)
{
	for (var i = 0; i < ballList.length; i++)
	{
		for (var j = 0; j < ballList.length; j++)
		{
			if (i != j)
			{
				let collision = Matter.Collision.collides(ballList[i], ballList[j]);

				if (collision != null)
				{
					let collisionImpact = ballList[i].speed + ballList[i].speed;
					let soundVol = map(collisionImpact, 1, 20, 0, 1);
					ballToBallSound.setVolume(soundVol); // Extension: The ball to ball sound is dependent on the impact of the collision
					ballToBallSound.play();
					ballToBallSound.setVolume(1);
				}
			}
		}
	}
}

function cueCollision(body)
{
	let collision = Matter.Collision.collides(body, cueStick);

	let magnitude = Math.sqrt((body.position.x - cueStick.position.x)**2 + (body.position.y - cueStick.position.y)**2);
	let unit_vector= {x: (body.position.x - cueStick.position.x) / magnitude, y: (body.position.y - cueStick.position.y) / magnitude};


	if (collision != null)
	{
		let pointOfCollision = {x: collision.supports[0].x, y: collision.supports[0].y};

		if (dist(pointOfCollision.x, pointOfCollision.y, cueStick.position.x, cueStick.position.y) > 100 && body.speed < 0.01)
		{
			cueToBallSound.play(); // Extension: sound when cue stick hits the cue ball
			let force_mag = collision.depth / 37.5;
			Body.applyForce(body, {x: body.position.x, y: body.position.y}, {x: force_mag * unit_vector.x, y: force_mag * unit_vector.y});
		}
	}
}

function mouseWheel(event) // rotate cue stick with mouse wheel
{
	if (event.delta > 0)
		Body.rotate(cueStick, Math.PI / 36);
	else
		Body.rotate(cueStick, -Math.PI / 36);
}

function ballinHole(ball) // returns true if ball is in one of the holes
{
	for (var i = 0; i < holes.length; i++)
	{
		if (dist(ball.position.x, ball.position.y, holes[i].position.x, holes[i].position.y) < 8)
			return true;
	}
}

function maxSpeed(ball) // stops ball from increasing the ball speed more than 10
{
	if (ball.speed > 12)
	{
		Body.setSpeed(ball, 12);
	}
}

/**
	Manually applied friction, this function is also usable but I decided to use frictionAir for friction with the table.
	Table is not a World body here, so the frictionAir property works fine for friction with the table.
 **/
function frictionTable(ball)
{
	let opposingUnitVector = {x: -ball.velocity.x / ball.speed, y: -ball.velocity.y / ball.speed};
	
	if (ball.speed > 10)
	{
		Body.setSpeed(ball, 10);
	}

	if (ball.speed > 0)
		Body.applyForce(ball, {x: ball.position.x, y: ball.position.y}, {x: 0.0001 * opposingUnitVector.x, y: 0.0001 * opposingUnitVector.y});
}

// Example is based on examples from:
// http://brm.io/matter-js/
// https://github.com/shiffman/p5-matter
// https://github.com/b-g/p5-matter-examples

// module aliases
var Engine = Matter.Engine;
var Render = Matter.Render;
var World = Matter.World;
var Bodies = Matter.Bodies;
var Composites = Matter.Composites;
var Body = Matter.Body;
var Detector = Matter.Detector;

var engine;
var cueToBallSound;
var ballSinkSound;
var ballToBallSound;

var start=false; // Has the game started?
var mode;
var totalScore = 0;


// loading the sounds
function preload() {
	soundFormats('mp3');
	cueToBallSound = loadSound('sounds/snooker_sound.mp3');
	ballSinkSound = loadSound('sounds/ballSink.mp3');
	ballToBallSound = loadSound('sounds/ballToBall.mp3');
}

function keyPressed()
{
	// the three modes
	if (keyCode == 83)
		mode = "proper"; // proper start
	if (keyCode == 82)
		mode = "random"; // all balls are random but the cue ball
	if (keyCode == 84)
		mode = "randomRed"; // red balls are random
	if (keyCode == 82 || keyCode == 83 || keyCode == 84)
	{
		start = true;
		totalScore = 0; //reset score
		World.remove(engine.world, engine.world.bodies); // remove all to start a new game
		setupTable();
		setupObjects();
	}
}

function setup() {
	createCanvas(1200, 800);

	// text on screen
	textSize(24);

	// create an engine
	engine = Engine.create();

	// no need of gravity
	engine.gravity.y = 0;

	setupTable();
}

/////////////////////////////////////////////////////////
function draw() {
	background(120);
	Engine.update(engine);

	drawTable();

	noStroke();
	fill(0);
	text("Press s for a standard start", width / 2 - 100, 50);
	text("Press r to generate random position for all balls.", width / 2 - 200, 100);
	text("Press t to generate random position for red balls.", width / 2 - 200, 150);

	if (start)
	{
		drawObjects();
		noStroke();
		fill(0);
		text("Scroll for adjusting the cue stick angle", width / 2 - 200, height - 100);
		text("Press c for cheats", width / 2 - 100, height - 50);
		if (keyCode == 67)
		{
			noStroke();
			fill(0, 0, 200);
			textSize(16);
			text("Press arrow keys for adjusting the position of cue ball", 10, 50);
			textSize(24);
		}
		fill(255);
		text("Score: " + totalScore.toString(), 50, height / 2); // display total score
	}
	else
	{
		noStroke();
		fill(0);
	}
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
// ********* HELPER FUNCTIONS **********
/////////////////////////////////////////////////////////
function drawVertices(vertices) {
	beginShape();
	for (var i = 0; i < vertices.length; i++) {
		vertex(vertices[i].x, vertices[i].y);
	}
	endShape(CLOSE);
}

function drawCircle(center, radius) //draw a proper circle with p5.js circle function
{
	beginShape();
	circle(center.x, center.y, radius * 2);
	endShape(CLOSE);
}

var table;
//var ball;
var rails = [];
var holes = [];
var corners = [];
var cushions = [];
var tableWidth = 800;
var tableHeight = 400;
var Line;

function setupHole(center, vertex, dist, radius)
{
	let sign1, sign2;
	sign1 = (center.x - vertex.x) / Math.abs(center.x - vertex.x);
	sign2 = (center.y - vertex.y) / Math.abs(center.y - vertex.y);

	if (sign1.toString() == "NaN")
		sign1 = 0;
	if (sign2.toString() == "NaN")
		sign2 = 0;
	let hole = Bodies.circle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, radius, {isStatic: true});
	//World.add(engine.world, [hole]);
	return hole;
}

function setupRails()
{
	// rails at top
	rails.push(Bodies.rectangle(
		table.position.x - tableWidth / 4,
		table.vertices[0].y - 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.position.x + tableWidth / 4,
		table.vertices[1].y - 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	// rails at bottom
	rails.push(Bodies.rectangle(
		table.position.x + tableWidth / 4,
		table.vertices[2].y + 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.position.x - tableWidth / 4,
		table.vertices[2].y + 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	// rails at sides
	rails.push(Bodies.rectangle(
		table.vertices[0].x - 7.5,
		table.position.y,
		15, 
		tableHeight - 20,
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.vertices[1].x + 7.5,
		table.position.y,
		15, 
		tableHeight - 20,
		{isStatic: true}
	));
}

function setupCorners(center, vertex, dist, side1, side2, options)
{
	let sign1, sign2;
	sign1 = -1 * (center.x - vertex.x) / Math.abs(center.x - vertex.x);
	sign2 = -1 * (center.y - vertex.y) / Math.abs(center.y - vertex.y);

	if (sign1.toString() == "NaN")
		sign1 = 0;
	if (sign2.toString() == "NaN")
		sign2 = 0;

	//return Bodies.rectangle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, side, side, {isStatic: true, chamfer: {radius: [10, 0, 0, 0]}});
	return Bodies.rectangle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, side1, side2, options);
}

function setupCushion(center, type, cushionLength)
{
	let biggerSide = tableWidth / 2 - 27;
	let options = {isStatic: true};
	if (type == 1)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - biggerSide / 2, y: center.y - cushionLength / 2},
				{x: center.x + biggerSide / 2, y: center.y - cushionLength / 2},
				{x: center.x + (biggerSide - 20) / 2, y: center.y + cushionLength / 2},
				{x: center.x - (biggerSide - 20) / 2, y: center.y + cushionLength / 2},
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else if (type == 2)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - (biggerSide - 20) / 2, y: center.y - cushionLength / 2},
				{x: center.x + (biggerSide - 20) / 2, y: center.y - cushionLength / 2},
				{x: center.x + biggerSide / 2, y: center.y + cushionLength / 2},
				{x: center.x - biggerSide / 2, y: center.y + cushionLength / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else if (type == 3)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - cushionLength / 2, y: center.y - biggerSide / 2},
				{x: center.x + cushionLength / 2, y: center.y - (biggerSide - 20) / 2},
				{x: center.x + cushionLength / 2, y: center.y + (biggerSide - 20) / 2},
				{x: center.x - cushionLength / 2, y: center.y + biggerSide / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - cushionLength / 2, y: center.y - (biggerSide - 20) / 2},
				{x: center.x + cushionLength / 2, y: center.y - biggerSide / 2},
				{x: center.x + cushionLength / 2, y: center.y + biggerSide / 2},
				{x: center.x - cushionLength / 2, y: center.y + (biggerSide - 20) / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
}

function setupTable()
{
	Line = [
		{x: width / 2 - 5 * tableWidth / 18, y: height / 2 - tableHeight / 2 + 11},
		{x: width / 2 - 5 * tableWidth / 18, y: height / 2 + tableHeight / 2 - 11}
	];

	//table = Bodies.rectangle(width / 2, height / 2, 800, 400, {isStatic: true, friction: 0.1, restitution: 0.8});
	table = Bodies.rectangle(width / 2, height / 2, 800, 400);

	// holes and corners at vertices
	var rounded_corners = [0, 0, 0, 0];
	for (var i = 0; i < 4; i++)
	{
		rounded_corners[i] += 10;
		holes.push(setupHole(table.position, table.vertices[i], 7.071, 10));
		corners.push(setupCorners(table.position, table.vertices[i], 2.5, 25, 25, {isStatic: true, chamfer: {radius: rounded_corners}}));
		rounded_corners[i] -= 10;
	}
	
	// holes and corners in between the rails
	holes.push(setupHole(table.position, {x: table.position.x, y: table.position.y - tableHeight / 2}, 0, 10));
	holes.push(setupHole(table.position, {x: table.position.x, y: table.position.y + tableHeight / 2}, 0, 10));
	corners.push(setupCorners(table.position, {x: table.position.x, y: table.position.y - tableHeight / 2 - 7.5}, 0, 20, 15, {isStatic: true}));
	corners.push(setupCorners(table.position, {x: table.position.x, y: table.position.y + tableHeight / 2 + 7.5}, 0, 20, 15, {isStatic: true}));

	// setup rails
	setupRails();

	// setup cushions
	// top
	setupCushion({x: table.position.x - tableWidth / 4, y: table.vertices[0].y + 5}, 1, 10);
	setupCushion({x: table.position.x + tableWidth / 4, y: table.vertices[0].y + 5}, 1, 10);
	// bottom
	setupCushion({x: table.position.x + tableWidth / 4, y: table.vertices[2].y - 5}, 2, 10);
	setupCushion({x: table.position.x - tableWidth / 4, y: table.vertices[2].y - 5}, 2, 10);
	// sides
	setupCushion({x: table.vertices[0].x + 5, y: table.position.y}, 3, 10);
	setupCushion({x: table.vertices[1].x - 5, y: table.position.y}, 4, 10);

	//World.add(engine.world, [table, holes, rails, cushions]);
	//World.add(engine.world, [table]);
}

function drawTable()
{
	noStroke();
	fill(34, 139, 34);
	drawVertices(table.vertices);

	// corners
	noStroke();
	fill(255, 215, 0);
	for (var i = 0; i < corners.length; i++)
	{
		drawVertices(corners[i].vertices);
	}

	// rails
	noStroke();
	fill(74, 33, 6);
	for (var i = 0; i < rails.length; i++)
	{
		drawVertices(rails[i].vertices);
	}

	// holes
	noStroke();
	fill(0);
	for (var i = 0; i < holes.length; i++)
	{
		drawCircle({x: holes[i].position.x, y: holes[i].position.y}, holes[0].circleRadius);
	}

	// cushions 
	noStroke();
	fill(1, 100, 1);
	for (var i = 0; i < cushions.length; i++)
	{
		drawVertices(cushions[i].vertices);
	}

	//lines
	stroke(225);
	noFill();
	line(Line[0].x, Line[0].y, Line[1].x, Line[1].y);
	arc(Line[0].x, table.position.y, 150, 150, PI/2, 3 * PI/2);

}
