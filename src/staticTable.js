var table;
//var ball;
var rails = [];
var holes = [];
var corners = [];
var cushions = [];
var tableWidth = 800;
var tableHeight = 400;
var Line;

function setupHole(center, vertex, dist, radius)
{
	let sign1, sign2;
	sign1 = (center.x - vertex.x) / Math.abs(center.x - vertex.x);
	sign2 = (center.y - vertex.y) / Math.abs(center.y - vertex.y);

	if (sign1.toString() == "NaN")
		sign1 = 0;
	if (sign2.toString() == "NaN")
		sign2 = 0;
	let hole = Bodies.circle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, radius, {isStatic: true});
	//World.add(engine.world, [hole]);
	return hole;
}

function setupRails()
{
	// rails at top
	rails.push(Bodies.rectangle(
		table.position.x - tableWidth / 4,
		table.vertices[0].y - 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.position.x + tableWidth / 4,
		table.vertices[1].y - 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	// rails at bottom
	rails.push(Bodies.rectangle(
		table.position.x + tableWidth / 4,
		table.vertices[2].y + 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.position.x - tableWidth / 4,
		table.vertices[2].y + 7.5,
		tableWidth / 2 - 20,
		15, 
		{isStatic: true}
	));

	// rails at sides
	rails.push(Bodies.rectangle(
		table.vertices[0].x - 7.5,
		table.position.y,
		15, 
		tableHeight - 20,
		{isStatic: true}
	));

	rails.push(Bodies.rectangle(
		table.vertices[1].x + 7.5,
		table.position.y,
		15, 
		tableHeight - 20,
		{isStatic: true}
	));
}

function setupCorners(center, vertex, dist, side1, side2, options)
{
	let sign1, sign2;
	sign1 = -1 * (center.x - vertex.x) / Math.abs(center.x - vertex.x);
	sign2 = -1 * (center.y - vertex.y) / Math.abs(center.y - vertex.y);

	if (sign1.toString() == "NaN")
		sign1 = 0;
	if (sign2.toString() == "NaN")
		sign2 = 0;

	//return Bodies.rectangle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, side, side, {isStatic: true, chamfer: {radius: [10, 0, 0, 0]}});
	return Bodies.rectangle(vertex.x + sign1 * dist, vertex.y + sign2 * dist, side1, side2, options);
}

function setupCushion(center, type, cushionLength)
{
	let biggerSide = tableWidth / 2 - 27;
	let options = {isStatic: true};
	if (type == 1)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - biggerSide / 2, y: center.y - cushionLength / 2},
				{x: center.x + biggerSide / 2, y: center.y - cushionLength / 2},
				{x: center.x + (biggerSide - 20) / 2, y: center.y + cushionLength / 2},
				{x: center.x - (biggerSide - 20) / 2, y: center.y + cushionLength / 2},
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else if (type == 2)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - (biggerSide - 20) / 2, y: center.y - cushionLength / 2},
				{x: center.x + (biggerSide - 20) / 2, y: center.y - cushionLength / 2},
				{x: center.x + biggerSide / 2, y: center.y + cushionLength / 2},
				{x: center.x - biggerSide / 2, y: center.y + cushionLength / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else if (type == 3)
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - cushionLength / 2, y: center.y - biggerSide / 2},
				{x: center.x + cushionLength / 2, y: center.y - (biggerSide - 20) / 2},
				{x: center.x + cushionLength / 2, y: center.y + (biggerSide - 20) / 2},
				{x: center.x - cushionLength / 2, y: center.y + biggerSide / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
	else
	{
		let cushion = Bodies.fromVertices(
			center.x,
			center.y,
			[
				{x: center.x - cushionLength / 2, y: center.y - (biggerSide - 20) / 2},
				{x: center.x + cushionLength / 2, y: center.y - biggerSide / 2},
				{x: center.x + cushionLength / 2, y: center.y + biggerSide / 2},
				{x: center.x - cushionLength / 2, y: center.y + (biggerSide - 20) / 2}
			],
			options
		);
		cushion.restitution = 0.1;
		cushion.friction = 0.8;
		cushions.push(cushion);
		World.add(engine.world, [cushion]);
	}
}

function setupTable()
{
	Line = [
		{x: width / 2 - 5 * tableWidth / 18, y: height / 2 - tableHeight / 2 + 11},
		{x: width / 2 - 5 * tableWidth / 18, y: height / 2 + tableHeight / 2 - 11}
	];

	//table = Bodies.rectangle(width / 2, height / 2, 800, 400, {isStatic: true, friction: 0.1, restitution: 0.8});
	table = Bodies.rectangle(width / 2, height / 2, 800, 400);

	// holes and corners at vertices
	var rounded_corners = [0, 0, 0, 0];
	for (var i = 0; i < 4; i++)
	{
		rounded_corners[i] += 10;
		holes.push(setupHole(table.position, table.vertices[i], 7.071, 10));
		corners.push(setupCorners(table.position, table.vertices[i], 2.5, 25, 25, {isStatic: true, chamfer: {radius: rounded_corners}}));
		rounded_corners[i] -= 10;
	}
	
	// holes and corners in between the rails
	holes.push(setupHole(table.position, {x: table.position.x, y: table.position.y - tableHeight / 2}, 0, 10));
	holes.push(setupHole(table.position, {x: table.position.x, y: table.position.y + tableHeight / 2}, 0, 10));
	corners.push(setupCorners(table.position, {x: table.position.x, y: table.position.y - tableHeight / 2 - 7.5}, 0, 20, 15, {isStatic: true}));
	corners.push(setupCorners(table.position, {x: table.position.x, y: table.position.y + tableHeight / 2 + 7.5}, 0, 20, 15, {isStatic: true}));

	// setup rails
	setupRails();

	// setup cushions
	// top
	setupCushion({x: table.position.x - tableWidth / 4, y: table.vertices[0].y + 5}, 1, 10);
	setupCushion({x: table.position.x + tableWidth / 4, y: table.vertices[0].y + 5}, 1, 10);
	// bottom
	setupCushion({x: table.position.x + tableWidth / 4, y: table.vertices[2].y - 5}, 2, 10);
	setupCushion({x: table.position.x - tableWidth / 4, y: table.vertices[2].y - 5}, 2, 10);
	// sides
	setupCushion({x: table.vertices[0].x + 5, y: table.position.y}, 3, 10);
	setupCushion({x: table.vertices[1].x - 5, y: table.position.y}, 4, 10);

	//World.add(engine.world, [table, holes, rails, cushions]);
	//World.add(engine.world, [table]);
}

function drawTable()
{
	noStroke();
	fill(34, 139, 34);
	drawVertices(table.vertices);

	// corners
	noStroke();
	fill(255, 215, 0);
	for (var i = 0; i < corners.length; i++)
	{
		drawVertices(corners[i].vertices);
	}

	// rails
	noStroke();
	fill(74, 33, 6);
	for (var i = 0; i < rails.length; i++)
	{
		drawVertices(rails[i].vertices);
	}

	// holes
	noStroke();
	fill(0);
	for (var i = 0; i < holes.length; i++)
	{
		drawCircle({x: holes[i].position.x, y: holes[i].position.y}, holes[0].circleRadius);
	}

	// cushions 
	noStroke();
	fill(1, 100, 1);
	for (var i = 0; i < cushions.length; i++)
	{
		drawVertices(cushions[i].vertices);
	}

	//lines
	stroke(225);
	noFill();
	line(Line[0].x, Line[0].y, Line[1].x, Line[1].y);
	arc(Line[0].x, table.position.y, 150, 150, PI/2, 3 * PI/2);

}
