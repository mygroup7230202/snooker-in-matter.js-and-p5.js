/** 
	Class for handling all the balls
**/

class AllBalls
{
	ballOptions = {restitution: 0.8, friction: 0.1, density: 0.01, frictionAir: 0.02};

	#white = "#ffffff";
	#red = "#c80000";
	#colors = [
		"#dcdc12",
		"#015501",
		"#894514",
		"#0000c9",
		"#d45c98",
		"#000000"
	];

	cueBall;
	redBalls = [];
	coloredBalls = [];

	move = Moves.red;

	constructor(redBallPositions, coloredBallPositions)
	{
		this.cueBall = new Ball(this.#white, {x: Line[0].x - 75, y: table.position.y}, this.ballOptions, -1);

		for (var i = 0; i < 15; i++)
			this.redBalls.push(new Ball(this.#red, redBallPositions[i], this.ballOptions, 1));

		for (var i = 0; i < 6; i++)
			this.coloredBalls.push(new Ball(this.#colors[i], coloredBallPositions[i], this.ballOptions, i + 2));
	}

	drawAll() // draw all balls
	{
		this.cueBall.draw();

		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].draw();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			this.coloredBalls[i].draw();
		}
	}

	addAlltoWorld() // add all balls to the world
	{
		this.cueBall.addtoWorld();

		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].addtoWorld();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
			this.coloredBalls[i].addtoWorld();
	}

	/**
	recordAllPositions() // record colored ball positions
	{
		for (var i = 0; i < this.coloredBalls.length; i++)
			this.coloredBalls[i].recordPosition(this.coloredBalls[i].body.position);
	}
	**/

	regulateSpeedAll()
	{
		this.cueBall.regulateSpeed();
		for (var i = 0; i < this.redBalls.length; i++)
		{
			this.redBalls[i].regulateSpeed();
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
			this.coloredBalls[i].regulateSpeed();
	}

	ballSink()
	{
		if (ballinHole(this.cueBall.body))
		{
			Body.setPosition(this.cueBall.body, this.cueBall.position);
			Body.setVelocity(this.cueBall.body, {x: 0, y: 0});
			totalScore -= 2;
			ballSinkSound.play(); //Extension: Sound when the ball sinks
		}

		for (var i = 0; i < this.redBalls.length; i++)
		{
			if (ballinHole(this.redBalls[i].body) && this.move == Moves.red)
			{
				this.redBalls[i].removeFromWorld();
				this.redBalls.splice(i, 1);
				ballSinkSound.play();
				totalScore++;
				this.move = Moves.colored;
			}
			else if (ballinHole(this.redBalls[i].body) && this.move == Moves.colored)
			{
				ballSinkSound.play();
				Body.setPosition(this.redBalls[i].body, this.redBalls[i].position);
				Body.setVelocity(this.redBalls[i].body, {x: 0, y: 0});
				totalScore--;
			}
		}

		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			if (ballinHole(this.coloredBalls[i].body))
			{
				if (this.move == Moves.colored)
				{
					if (this.redBalls.length == 0)
					{
						ballSinkSound.play();
						totalScore += this.coloredBalls[i].score;
						this.coloredBalls[i].removeFromWorld();
						this.coloredBalls.splice(i, 1);
						if (this.coloredBalls.length == 0)
						{
							start = "over";
							gameWinSound.play();
						}
					}
					else
					{
						ballSinkSound.play();
						totalScore += this.coloredBalls[i].score;
						Body.setPosition(this.coloredBalls[i].body, this.coloredBalls[i].position);
						Body.setVelocity(this.coloredBalls[i].body, {x: 0, y: 0});
						this.move = Moves.red;
					}
				}
				else
				{
					ballSinkSound.play();
					Body.setPosition(this.coloredBalls[i].body, this.coloredBalls[i].position);
					Body.setVelocity(this.coloredBalls[i].body, {x: 0, y: 0});
					totalScore--;
				}
			}
		}
	}

	ballsInMotion()
	{
		let motionBalls = []
		if (this.cueBall.isInMotion())
			motionBalls.push(this.cueBall.body);

		for (var i = 0; i < this.redBalls.length; i++)
		{
			if(this.redBalls[i].isInMotion())
				motionBalls.push(this.redBalls[i].body)
		}
		for (var i = 0; i < this.coloredBalls.length; i++)
		{
			if(this.coloredBalls[i].isInMotion())
				motionBalls.push(this.coloredBalls[i].body)
		}
		return motionBalls;
	}
};
