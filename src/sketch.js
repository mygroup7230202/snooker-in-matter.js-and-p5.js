// Example is based on examples from:
// http://brm.io/matter-js/
// https://github.com/shiffman/p5-matter
// https://github.com/b-g/p5-matter-examples

// module aliases
var Engine = Matter.Engine;
var Render = Matter.Render;
var World = Matter.World;
var Bodies = Matter.Bodies;
var Composites = Matter.Composites;
var Body = Matter.Body;
var Detector = Matter.Detector;

var engine;
var cueToBallSound;
var ballSinkSound;
var ballToBallSound;
var gameWinSound;

var start="no"; // Has the game started?
var mode;
var totalScore = 0;


// loading the sounds
function preload() {
	soundFormats('mp3');
	cueToBallSound = loadSound('sounds/snooker_sound.mp3');
	ballSinkSound = loadSound('sounds/ballSink.mp3');
	ballToBallSound = loadSound('sounds/ballToBall.mp3');
	gameWinSound = loadSound('sounds/gwin.mp3');
}

function keyPressed()
{
	// the three modes
	if (keyCode == 83)
		mode = "proper"; // proper start
	if (keyCode == 82)
		mode = "random"; // all balls are random but the cue ball
	if (keyCode == 84)
		mode = "randomRed"; // red balls are random
	if (keyCode == 82 || keyCode == 83 || keyCode == 84)
	{
		start = "yes";
		totalScore = 0; //reset score
		World.remove(engine.world, engine.world.bodies); // remove all to start a new game
		setupTable();
		setupObjects();
	}
}

function setup() {
	createCanvas(1200, 800);

	// text on screen
	textSize(24);

	// create an engine
	engine = Engine.create();

	// no need of gravity
	engine.gravity.y = 0;

	setupTable();
}

/////////////////////////////////////////////////////////
function draw() {
	background(120);
	Engine.update(engine);

	drawTable();

	noStroke();
	fill(0);
	text("Press s for a standard start", width / 2 - 100, 50);
	text("Press r to generate random position for all balls.", width / 2 - 200, 100);
	text("Press t to generate random position for red balls.", width / 2 - 200, 150);

	if (start == "yes")
	{
		drawObjects();
		noStroke();
		fill(0);
		text("Scroll for adjusting the cue stick angle", width / 2 - 200, height - 100);
		text("Press c for cheats", width / 2 - 100, height - 50);
		if (keyCode == 67)
		{
			noStroke();
			fill(0, 0, 200);
			textSize(16);
			text("Press arrow keys for adjusting the position of cue ball", 10, 50);
			textSize(24);
		}
		fill(255);
		text("Score: " + totalScore.toString(), 50, height / 2); // display total score
	}
	else if (start == "over")
	{
		noStroke();
		fill("#00ff00");
		text("You Win! Score: " + totalScore.toString(), width / 2 - 200, height - 150);
	}
}

/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
// ********* HELPER FUNCTIONS **********
/////////////////////////////////////////////////////////
function drawVertices(vertices) {
	beginShape();
	for (var i = 0; i < vertices.length; i++) {
		vertex(vertices[i].x, vertices[i].y);
	}
	endShape(CLOSE);
}

function drawCircle(center, radius) //draw a proper circle with p5.js circle function
{
	beginShape();
	circle(center.x, center.y, radius * 2);
	endShape(CLOSE);
}
